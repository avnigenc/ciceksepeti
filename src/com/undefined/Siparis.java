package com.undefined;

public class Siparis {

    private String no;
    private String latitude;
    private String longitude;
    private Double distanceForRed;
    private Double distanceForGreen;
    private Double distanceForBlue;

    public Siparis(String no, String latitude, String longitude, Double distanceForRed, Double distanceForGreen, Double distanceForBlue) {
        this.no = no;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distanceForRed = distanceForRed;
        this.distanceForGreen = distanceForGreen;
        this.distanceForBlue = distanceForBlue;
    }

    public Double getDistanceForRed() {
        return distanceForRed;
    }

    public void setDistanceForRed(Double distanceForRed) {
        this.distanceForRed = distanceForRed;
    }

    public Double getDistanceForGreen() {
        return distanceForGreen;
    }

    public void setDistanceForGreen(Double distanceForGreen) {
        this.distanceForGreen = distanceForGreen;
    }

    public Double getDistanceForBlue() {
        return distanceForBlue;
    }

    public void setDistanceForBlue(Double distanceForBlue) {
        this.distanceForBlue = distanceForBlue;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


}
