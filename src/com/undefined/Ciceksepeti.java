package com.undefined;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Ciceksepeti {

    private static ArrayList<String> redList;
    private static ArrayList<String> greenList;
    private static ArrayList<String> blueList;

    private static ArrayList<String> newRedList;
    private static ArrayList<String> newGreenList;
    private static ArrayList<String> newBlueList;

    private static ArrayList<String> allOrders;
    private static ArrayList<String> newAllOrders;

    public static void main(String[] args) throws IOException, ParseException {


        JSONParser parser = new JSONParser();

        JSONArray data = (JSONArray) parser.parse(new FileReader("/home/avnigenc/Desktop/ciceksepeti/src/com/undefined/data.json"));
        JSONArray bayii = (JSONArray) parser.parse(new FileReader("/home/avnigenc/Desktop/ciceksepeti/src/com/undefined/bayii.json"));

        redList = new ArrayList<String>(30);
        greenList = new ArrayList<String>(50);
        blueList = new ArrayList<String>(80);

        newRedList = new ArrayList<String>(29);
        newGreenList = new ArrayList<String>(35);
        newBlueList = new ArrayList<String>(36);

        allOrders = new ArrayList<String>(100);
        newAllOrders = new ArrayList<String>(100);



        for (Object o : data)
        {
            JSONObject jsonObject = (JSONObject) o;

            String no = (String) jsonObject.get("no");
            String latitude = (String) jsonObject.get("latitude");
            String longitude = (String) jsonObject.get("longitude");

            jsonObject.put("distanceForRed",distanceInKmBetweenRed(latitude, longitude, no));
            jsonObject.put("distanceForGreen",distanceInKmBetweenGreen(latitude, longitude, no));
            jsonObject.put("distanceForBlue",distanceInKmBetweenBlue(latitude, longitude, no));

            double red = (Double) jsonObject.get("distanceForRed");
            double green = (Double) jsonObject.get("distanceForGreen");
            double blue = (Double) jsonObject.get("distanceForBlue");




            if(shortestRoute(red, green, blue) == 0){
                redList.add(jsonObject.toJSONString());
            }else if(shortestRoute(red, green, blue) == 1){
                greenList.add(jsonObject.toJSONString());
            }else {
                blueList.add(jsonObject.toJSONString());
            }
        }

        for (Object o : bayii){
            JSONObject jsonObject = (JSONObject) o ;

            String latitude = (String) jsonObject.get("latitude");
            String longitude = (String) jsonObject.get("longitude");
            String color = (String) jsonObject.get("color");
            if(color.equals("Kırmızı")){
                System.out.println(latitude + "\t" + longitude + "\t" + "star5" + "\t" + "red" + "\t" + 5);
            }else if(color.equals("Yeşil")){
                System.out.println(latitude + "\t" + longitude + "\t" + "star5" + "\t" + "green" + "\t" + 5);
            }else {
                System.out.println(latitude + "\t" + longitude + "\t" + "star5" + "\t" + "blue" + "\t" + 5);
            }
        }


        //TODO sort arraylist by spec index
/*        System.out.println(redList);
        Collections.sort(redList);
        System.out.println(redList);*/


        newRedList.addAll(redList);
        newBlueList.addAll(blueList);
        newBlueList.addAll(greenList.subList(0, 28));
        newGreenList.addAll(greenList.subList(28, 62));


        for (Object o : newRedList){
            JSONParser parser1 = new JSONParser();
            JSONObject json = (JSONObject) parser1.parse(o.toString());
            json.get("latitude");
            json.get("longitude");
            System.out.println(json.get("latitude") + "\t" + json.get("longitude") + "\t" +  "circle3" + "\t" + "red" + "\t" + 3);
        }

        for (Object o : newGreenList){
            JSONParser parser1 = new JSONParser();
            JSONObject json = (JSONObject) parser1.parse(o.toString());
            json.get("latitude");
            json.get("longitude");
            System.out.println(json.get("latitude") + "\t" + json.get("longitude") + "\t" +  "circle3" + "\t" + "green" + "\t" + 3);
        }

        for (Object o : newBlueList){
            JSONParser parser1 = new JSONParser();
            JSONObject json = (JSONObject) parser1.parse(o.toString());
            json.get("latitude");
            json.get("longitude");
            System.out.println(json.get("latitude") + "\t" + json.get("longitude") + "\t" +  "circle3" + "\t" + "blue" + "\t" + 3);
        }

/*        Collections.sort(red);
        List<String> redOrders = red.subList(0, 30);
        System.out.println(redOrders.size());
        ArrayList<String> newRedList = new ArrayList<String>();
        for(String temp : redOrders)
        {
            newRedList.add(temp.substring(temp.length() - 5));
        }
        System.out.println(newRedList);


        Collections.sort(green);
        List<String> greenOrders = green.subList(0, 50);
        System.out.println(greenOrders.size());
        ArrayList<String> newGreenList = new ArrayList<String>();
        for(String temp : greenOrders)
        {
            newGreenList.add(temp.substring(temp.length() - 5));
        }
        System.out.println(newGreenList);


        Collections.sort(blue);
        List<String> blueOrders = blue.subList(0, 20);
        System.out.println(blueOrders.size());
        ArrayList<String> newBlueList = new ArrayList<String>();
        for(String temp : blueOrders)
        {
            newBlueList.add(temp.substring(temp.length() - 5));
        }
        System.out.println(newBlueList);



        for (String temp : newRedList)
        {
            allOrders.remove(temp);
        }
        System.out.println(allOrders.size());

        for (String temp : newGreenList)
        {
            allOrders.remove(temp);
        }
        System.out.println(allOrders.size());*/


    }

    private static int shortestRoute(double red, double green, double blue){
        ArrayList<Double> arrli = new ArrayList<Double>();

        arrli.add(red);
        arrli.add(green);
        arrli.add(blue);

        return minIndex(arrli);
    }

    private static int shortestRouteForTwo(double x, double y){
        ArrayList<Double> arrli = new ArrayList<Double>();
        arrli.add(x);
        arrli.add(y);
        return minIndex(arrli);
    }

    private static int minIndex(ArrayList<Double> list) {
        return list.indexOf(Collections.min(list));
    }

    private static double distanceInKmBetweenRed(String lat1, String long1, String no){
        int earthRadiusKm = 6371;

        double dLat = degressToRadians(Double.parseDouble("41.049792") - Double.parseDouble(lat1));
        double dLong = degressToRadians(Double.parseDouble("29.003031") - Double.parseDouble(long1));

        double lat11 = degressToRadians(Double.parseDouble(lat1));
        double lat2 = degressToRadians(Double.parseDouble("41.049792"));

        double a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLong / 2) * Math.sin(dLong / 2) * Math.cos(lat11) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1- a));

        return earthRadiusKm *c;

    }

    private static double distanceInKmBetweenBlue(String lat1, String long1, String no){
        int earthRadiusKm = 6371;

        double dLat = degressToRadians(Double.parseDouble("41.049997") - Double.parseDouble(lat1));
        double dLong = degressToRadians(Double.parseDouble("29.026108") - Double.parseDouble(long1));

        double lat11 = degressToRadians(Double.parseDouble(lat1));
        double lat2 = degressToRadians(Double.parseDouble("41.049997"));

        double a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLong / 2) * Math.sin(dLong / 2) * Math.cos(lat11) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1- a));

        return earthRadiusKm * c;
    }

    private static double distanceInKmBetweenGreen(String lat1, String long1, String no){
        int earthRadiusKm = 6371;

        double dLat = degressToRadians(Double.parseDouble("41.069940") - Double.parseDouble(lat1));
        double dLong = degressToRadians(Double.parseDouble("29.019250") - Double.parseDouble(long1));

        double lat11 = degressToRadians(Double.parseDouble(lat1));
        double lat2 = degressToRadians(Double.parseDouble("41.069940"));

        double a = Math.sin(dLat / 2) * Math.sin(dLat /2) + Math.sin(dLong / 2) * Math.sin(dLong / 2) * Math.cos(lat11) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1- a));

        return earthRadiusKm * c;
    }

    private static double degressToRadians(double degress){
        return (degress * Math.PI) / 180;
    }
}
