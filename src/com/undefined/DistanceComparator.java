package com.undefined;

import java.util.Comparator;

class DistanceComparator implements Comparator<Siparis> {

    public int compare(Siparis e1, Siparis e2){
        return e1.getDistanceForRed().compareTo(e2.getDistanceForRed());
    }

    @Override
    public Comparator<Siparis> reversed() {
        return null;
    }

    public String toString(){
        return "DistanceComparator";
    }
}
