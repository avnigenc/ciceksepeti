package com.undefined;

public class Bayii {

    private String color;
    private String latitude;
    private String longitude;
    private int min;
    private int max;

    public Bayii(String color, String latitude, String longitude, int min, int max) {
        this.color = color;
        this.latitude = latitude;
        this.longitude = longitude;
        this.min = min;
        this.max = max;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}
